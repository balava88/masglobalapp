﻿using Test.WebApi.App_Start;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;

namespace Test.WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            MappConfig.Init();
            StructuremapWebApi.Start();
        }

    }
}
