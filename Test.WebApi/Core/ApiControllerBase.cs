﻿using System;
using System.Net;
using System.Security;
using System.Web.Http;

namespace Test.WebApi.Core
{
    public class ApiControllerBase : ApiController
    {

        /// <summary>
        /// Wrapper method that allows standar error handling and validates if the user is autenticated
        /// </summary>
        /// <param name="codeToExecute">Piece of code you want wrap</param>
        /// <returns>Returns IHttpActionResult with the response</returns>
        protected IHttpActionResult GetHttpResponseWithAutentication(Func<IHttpActionResult> codeToExecute)
        {
            if (User.Identity.IsAuthenticated)
            {
                return Content(HttpStatusCode.Unauthorized, "Not an Authenticated user");
            }

            IHttpActionResult response;

            try
            {
                response = ModelState.IsValid ? codeToExecute.Invoke() : BadRequest(ModelState);
            }
            catch (SecurityException)
            {
                response = Unauthorized();
            }
            catch (Exception ex)
            {
                response = InternalServerError(ex);
            }

            return response;
        }



        /// <summary>
        /// Wrapper method that allows standar error handling
        /// </summary>
        /// <param name="codeToExecute">Piece of code you want wrap</param>
        /// <returns>Returns IHttpActionResult with the response</returns>
        protected IHttpActionResult GetHttpResponse(Func<IHttpActionResult> codeToExecute)
        {
            IHttpActionResult response;

            try
            {
                response = codeToExecute.Invoke();
            }
            catch (SecurityException)
            {
                response = Unauthorized();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                return BadRequest(ModelState);
            }

            return response;
        }
    }
}