﻿using AutoMapper;
using Test.Common.DTO;
using Test.Common.Entities;
using Test.WebApi.ViewModels;

namespace Test.WebApi.App_Start
{
    public class MappConfig: AutoMapper.Profile
    {

        public static void Init()
        {
            Mapper.Initialize(config =>
            {

                config.CreateMap<Employee, EmployeeViewModel>().ReverseMap();
                config.CreateMap<EmployeeDTO, Employee>().ReverseMap();

            });
            //Mapper.Configuration.AssertConfigurationIsValid();
        }
    }
}