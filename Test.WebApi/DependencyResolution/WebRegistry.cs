using Test.BusinessContracts.Contracts;
using StructureMap;
using Test.DataContracts.Contracts;
using Test.Data.Rest.Repository;
using Test.Business.Logic;
using Test.Data.SQL.Repository;

namespace Test.WebApi.DependencyResolution
{

    public class WebRegistry : Registry {

        public WebRegistry() {

            //mapping between interfaces and implementations, in dal

            //uncomment In order to use Third Party API rest repository implementation
            For<IEmployeeRepository>().Use<EmployeeRepository>();

            //uncomment In order to use EF and local SQl db Repository
            //For<IEmployeeRepository>().Use<EmployeeSqlRepository>();



            For<IEmployeesBll>().Use<EmployeesBll>();            

        }

    }
}