using Test.BusinessContracts.Contracts;
using StructureMap;
using Test.DataContracts.Contracts;
using Test.Data.Rest.Repository;
using Test.Business.Logic;
using Test.Data.SQL.Repository;

namespace Test.WebApi.DependencyResolution
{

    public class DefaultRegistry : Registry {
        #region Constructors and Destructors

        public DefaultRegistry() {
            Scan(
                scan => {
                    scan.TheCallingAssembly();
                    scan.IncludeNamespace("Test.WebApi");
                    scan.IncludeNamespace("Test.DataContracts");
                    scan.IncludeNamespace("Test.Data.Rest");
                    scan.IncludeNamespace("Test.Data.SQL");
                    scan.IncludeNamespace("Test.Business");
                    scan.IncludeNamespace("Test.Common");
                    scan.IncludeNamespace("Test.Core");
                    scan.IncludeNamespace("Test.BusinessContracts");
                    scan.WithDefaultConventions();
					scan.With(new ControllerConvention());
                });      
        }

        #endregion
    }
}