﻿using Test.BusinessContracts.Contracts;
using Test.WebApi.Core;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Test.WebApi.ViewModels;
using AutoMapper;
using Test.Common.DTO;

namespace Test.WeApi.Controllers
{
    [RoutePrefix("api/Employees")]
    public class EmployeesController : ApiControllerBase
    {
        private readonly IEmployeesBll _employeeBll;

        public EmployeesController(IEmployeesBll employeeBll)
        {
            _employeeBll = employeeBll;
        }

        // GET: Employee
        public IHttpActionResult Get()
        {
            return GetHttpResponse(() =>
            {
                var employeesList = _employeeBll.GetAllEmployees().ToList();
                var employees = Mapper.Map<List<EmployeeDTO>,List<EmployeeViewModel>>(employeesList);

                return Ok(employees);

            });
        }

        // GET: Employee/id
        public IHttpActionResult Get(int id)
        {
            return GetHttpResponse(() =>
            {
                var employees = Mapper.Map<EmployeeDTO,EmployeeViewModel>(_employeeBll.GetEmployeeById(id));
                return Ok(employees);

            });
        }

    }
}