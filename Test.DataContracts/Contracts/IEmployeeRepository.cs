﻿using Test.Common.Entities;
using Test.Core.Contracts;

namespace Test.DataContracts.Contracts
{
    public interface IEmployeeRepository : IRepositoryBase<Employee>
    {
    }
}
