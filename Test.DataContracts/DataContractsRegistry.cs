﻿using StructureMap;

namespace Test.DataContracts
{
    public class DataContractsRegistry : Registry
    {
        public DataContractsRegistry()
        {
            Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.WithDefaultConventions();
            });
        }
    }
}
