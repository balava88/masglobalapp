﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Common.DTO;
using Test.Common.Entities;

namespace Test.Business.Logic
{
    public class HourlyEmployeeFactory: BaseEmployeeFactory
    {
        public HourlyEmployeeFactory(Employee employee) : base(employee)
        {
        }

        public override ISalaryCalculator CreateSalaryCalculator()
        {
            return new HourlySalaryCalculator();
        }
    }
}
