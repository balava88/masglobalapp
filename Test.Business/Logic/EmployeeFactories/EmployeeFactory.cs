﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Common.Entities;

namespace Test.Business.Logic.EmployeeFactories
{
    public class EmployeeFactory
    {
        public BaseEmployeeFactory CreateFactory(Employee employee)
        {
            BaseEmployeeFactory returnValue = null;

            switch (employee.ContractTypeName)
            {
                case "MonthlySalaryEmployee":
                    returnValue = new MonthlyEmployeeFactory(employee);
                    break;
                case "HourlySalaryEmployee":
                    returnValue = new HourlyEmployeeFactory(employee);
                    break;
            }

            return returnValue;
        }
    }
}
