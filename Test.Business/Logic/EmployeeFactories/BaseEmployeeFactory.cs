﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Test.Common.DTO;
using Test.Common.Entities;

namespace Test.Business.Logic
{
    public abstract class BaseEmployeeFactory
    {
        protected Employee _employee;

        protected BaseEmployeeFactory(Employee employee)
        {
            _employee = employee;
        }

        public EmployeeDTO GetEmployeeWithSalary()
        { 
            var employeeDto = Mapper.Map<EmployeeDTO>(_employee);

            var salaryCalculator = this.CreateSalaryCalculator();
            employeeDto.AnnualSalary = salaryCalculator.CalculateAnnualSalary(_employee.HourlySalary,_employee.MonthlySalary);
            return employeeDto;
        }

        public abstract ISalaryCalculator CreateSalaryCalculator();
    }
}
