﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Common.Entities;

namespace Test.Business.Logic
{
    public class MonthlyEmployeeFactory: BaseEmployeeFactory
    {
        public MonthlyEmployeeFactory(Employee employee) : base(employee)
        {
        }

        public override ISalaryCalculator CreateSalaryCalculator()
        {
            return new MonthlySalaryCalculator();
        }
    }
}
