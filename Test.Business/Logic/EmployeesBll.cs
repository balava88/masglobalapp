﻿using Test.BusinessContracts.Contracts;
using Test.DataContracts.Contracts;
using System;
using System.Linq;
using Test.Common.DTO;
using Test.Common.Entities;
using AutoMapper;
using System.Collections.Generic;
using Test.Business.Logic.EmployeeFactories;

namespace Test.Business.Logic
{
    public class EmployeesBll: IEmployeesBll
    {
        private readonly IEmployeeRepository _employeeRepo;

        public EmployeesBll(IEmployeeRepository employeeRepo)
        {
            _employeeRepo = employeeRepo;
        }
        
       
        /// <summary>
        /// Returns a list of employees Dto with the annual salary calculated
        /// </summary>
        /// <returns>List of EmployeeDto</returns>
        public IEnumerable<EmployeeDTO> GetAllEmployees()
        {
            
            try
            {
                var employeeList = _employeeRepo.Get();
                return employeeList.Select(EmployeeDto).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        /// <summary>
        /// Returns an Employee Dto based 
        /// </summary>
        /// <param name="employeeId">Employee Id</param>
        /// <returns>EmployeeDto</returns>
        public EmployeeDTO GetEmployeeById(int employeeId)
        {
            try
            {
                var employee = _employeeRepo.Get(employeeId);
                var result = EmployeeDto(employee);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static EmployeeDTO EmployeeDto(Employee employee)
        {
            if (employee == null) return null;
            var factory = new EmployeeFactory().CreateFactory(employee);
            return factory.GetEmployeeWithSalary();
        }
    }
}
