﻿namespace Test.Business.Logic
{
    public interface ISalaryCalculator
    {
        decimal CalculateAnnualSalary(decimal hourlySalary, decimal monthlySalary);
    }
}