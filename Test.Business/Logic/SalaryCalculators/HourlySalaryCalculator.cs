﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Business.Logic
{
    public class HourlySalaryCalculator: ISalaryCalculator
    {
        public decimal CalculateAnnualSalary(decimal hourlySalary, decimal monthlySalary)
        {
            return 120 * hourlySalary * 12;
        }
    }
}
