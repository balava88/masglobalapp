﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Business.Logic
{
    public class MonthlySalaryCalculator: ISalaryCalculator
    {
        public decimal CalculateAnnualSalary(decimal hourlySalary, decimal monthlySalary)
        {
            return monthlySalary * 12;
        }
    }
}
