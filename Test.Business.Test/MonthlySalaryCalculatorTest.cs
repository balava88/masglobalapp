﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Test.Business.Logic;

namespace Test.Business.Test
{
    [TestClass]
    public class MonthlySalaryCalculatorTest
    {
        [TestMethod]
        public void Valid_MonthlySalary_Calculation()
        {
            // arrange  
            
            decimal monthlySalary = 10;
            decimal hourlySalary = 0;
            decimal expectedAnnualSalary = 120;
            var monthlySalaryClaculator = new MonthlySalaryCalculator();

            // act  
            decimal annualSalary = monthlySalaryClaculator.CalculateAnnualSalary(hourlySalary, monthlySalary);
            
            // assert  
            Assert.AreEqual(expectedAnnualSalary, annualSalary);
        }

        [TestMethod]
        public void Invalid_MonthlySalary_Calculation()
        {
            // arrange  

            decimal monthlySalary = 15;
            decimal hourlySalary = 0;
            decimal expectedAnnualSalary = 120;
            var monthlySalaryClaculator = new MonthlySalaryCalculator();

            // act  
            decimal annualSalary = monthlySalaryClaculator.CalculateAnnualSalary(hourlySalary, monthlySalary);

            // assert  
            Assert.AreNotEqual(expectedAnnualSalary, annualSalary);
        }
    }
}
