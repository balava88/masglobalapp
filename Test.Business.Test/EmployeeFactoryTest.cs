﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Test.Business.Logic;
using Test.Business.Logic.EmployeeFactories;
using Test.Common.Entities;

namespace Test.Business.Test
{
    [TestClass]
    public class EmployeeFactoryTest
    {
        [TestMethod]
        public void Valid_MonthlyEmployeeFactory_TypeCreation_basedOn_ContractTypeName()
        {
            // arrange
            var employee = new Employee {ContractTypeName = "MonthlySalaryEmployee" };

            // act  
            var factory = new EmployeeFactory().CreateFactory(employee);

            // assert  
            Assert.IsInstanceOfType(factory, typeof(MonthlyEmployeeFactory));
        }


        [TestMethod]
        public void Invalid_MonthlyEmployeeFactory_TypeCreation_basedOn_ContractTypeName()
        {
            // arrange
            var employee = new Employee { ContractTypeName = "HourlySalaryEmployee" };

            // act  
            var factory = new EmployeeFactory().CreateFactory(employee);

            // assert  
            Assert.IsNotInstanceOfType(factory, typeof(MonthlyEmployeeFactory));
        }


        [TestMethod]
        public void Valid_HourlyEmployeeFactory_TypeCreation_basedOn_ContractTypeName()
        {
            // arrange
            var employee = new Employee { ContractTypeName = "HourlySalaryEmployee" };

            // act  
            var factory = new EmployeeFactory().CreateFactory(employee);

            // assert  
            Assert.IsInstanceOfType(factory, typeof(HourlyEmployeeFactory));
        }


        [TestMethod]
        public void Invalid_HourlyEmployeeFactory_TypeCreation_basedOn_ContractTypeName()
        {
            // arrange
            var employee = new Employee { ContractTypeName = "MonthlySalaryEmployee" };

            // act  
            var factory = new EmployeeFactory().CreateFactory(employee);

            // assert  
            Assert.IsNotInstanceOfType(factory, typeof(HourlyEmployeeFactory));
        }


    }
}
