﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Test.Business.Logic;

namespace Test.Business.Test
{
    [TestClass]
    public class HourlySalaryCalculatorTest
    {
        [TestMethod]
        public void Valid_HourlySalary_Calculation()
        {
            // arrange
            
            decimal monthlySalary = 0;
            decimal hourlySalary = 1; //1 * 120 * 12
            decimal expectedAnnualSalary = 1440;
            var monthlySalaryClaculator = new HourlySalaryCalculator();

            // act  

            decimal annualSalary = monthlySalaryClaculator.CalculateAnnualSalary(hourlySalary, monthlySalary);
            
            // assert  

            Assert.AreEqual(expectedAnnualSalary, annualSalary);
        }

        [TestMethod]
        public void Invalid_HourlySalary_Calculation()
        {
            // arrange  

            decimal monthlySalary = 0;
            decimal hourlySalary = 15;
            decimal expectedAnnualSalary = 120;
            var monthlySalaryClaculator = new HourlySalaryCalculator();

            // act  

            decimal annualSalary = monthlySalaryClaculator.CalculateAnnualSalary(hourlySalary, monthlySalary);

            // assert  

            Assert.AreNotEqual(expectedAnnualSalary, annualSalary);
        }
    }
}
