﻿using System.Collections.Generic;

namespace Test.Core.Contracts
{
    public interface IRepositoryBase<E>
        where E : Entity, new()
    {
        E Get(int id);
        List<E> Get();
        E Insert(E entity);
        void Delete(int id);
        E Update(E entity);
    }
}
