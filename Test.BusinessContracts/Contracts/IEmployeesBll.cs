﻿using System.Collections.Generic;
using Test.Common.DTO;

namespace Test.BusinessContracts.Contracts
{
    public interface IEmployeesBll
    {
        IEnumerable<EmployeeDTO> GetAllEmployees();
        EmployeeDTO GetEmployeeById(int employeeId);

    }
}
