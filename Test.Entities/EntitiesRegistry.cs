﻿using StructureMap;

namespace Test.Common
{
    public class EntitiesRegistry : Registry
    {
        public EntitiesRegistry()
        {
            Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.WithDefaultConventions();
            });

        }
    }
}
