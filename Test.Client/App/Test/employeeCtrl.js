﻿(function () {
    "use strict";

    angular
        .module("MasGlobalApp")
        .controller("EmployeeCtrl",["employeeResource", EmployeeCtrl]);

    function EmployeeCtrl(employeeResource) {

        var vm = this;
        vm.message = '';
        vm.employee = { id: '' };
        vm.employees = [];

        vm.Clear = function() {
            vm.employee.id = '';
            vm.message = undefined;
        };


        vm.Search = function () {

            if (vm.employee.id === '' ||  vm.employee.id === null) {

                //Query all the Employees
                vm.myPromise = employeeResource.query(null,
                    function (employees) {
                        vm.employees = employees;
                        vm.message = undefined;                        
                    },
                    function (response) {
                        vm.message = response.statusText + "\r\n";
                        if (response.data.exceptionMessage)
                            vm.message += response.data.exceptionMessage;
                        if (response.data.error)
                            vm.message += response.data.error;
                        if (response.data.error_description)
                            vm.message = response.data.error_description;
                    }).$promise;

            } else {

                //Query employee by id
                vm.myPromise = employeeResource.get({ id: vm.employee.id },
                    function (employee) {
                        vm.message = undefined;
                        vm.employees = [ employee ];
                        if (employee.id == undefined) vm.message = "Not Found";
                    },
                    function (response) {
                        vm.message = response.statusText + "\r\n";
                        if (response.data.exceptionMessage)
                            vm.message += response.data.exceptionMessage;
                        if (response.data.error)
                            vm.message += response.data.error;
                        if (response.data.error_description)
                            vm.message = response.data.error_description;
                    }).$promise;
            }

        };

    };

})();
