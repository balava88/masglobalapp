(function () {
    "use strict";

    var app = angular.module("MasGlobalApp",
                            ["common.services",
                             "ngSanitize",
                             "ui.router",
                             "cgBusy"]);


  


    app.config(["$stateProvider",
           "$urlRouterProvider",
           function ($stateProvider, $urlRouterProvider) {

               $urlRouterProvider.otherwise("/site/employees");

               $stateProvider
                  
                    .state("site", {
                        abstract: true,
                        url: "/site",
                        templateUrl: "App/siteView.html"
                   }) 
                   
                   .state("site.employees", {
                        url: "/employees",
                        templateUrl: "App/Test/employeeView.html",
                        controller: "EmployeeCtrl as vm"
                    })

           }]
    );

}());