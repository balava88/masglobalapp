﻿using System.Web.Mvc;
using System.Web.Routing;
using Test.Client.App_Start;

namespace Test.Client.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
