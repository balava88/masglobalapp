﻿using Test.Core.Contracts;
using Test.Core.Implementation;
using Test.Data.SQL.Models;

namespace Test.Data.SQL
{
    public class RepositoryBase<E> : RepositoryBase<E, TestContext>
       where E : Entity, new()
    {
    }
}
