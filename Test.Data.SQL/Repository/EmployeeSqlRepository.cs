﻿using Test.Common.Entities;
using Test.DataContracts.Contracts;

namespace Test.Data.SQL.Repository
{
    public class EmployeeSqlRepository : RepositoryBase<Employee>, IEmployeeRepository
    {
    }
}
