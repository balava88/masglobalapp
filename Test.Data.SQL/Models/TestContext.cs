namespace Test.Data.SQL.Models
{
    using Common.Entities;
    using System.Data.Entity;

    public partial class TestContext : DbContext
    {
        public TestContext()
            : base("name=TestModel")
        {
        }

        public virtual DbSet<Employee> Employee { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.ContractTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.RoleName)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.RoleDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.HourlySalary)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Employee>()
                .Property(e => e.MonthlySalary)
                .HasPrecision(18, 0);
        }
    }
}
