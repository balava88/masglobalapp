﻿using System;
using System.Collections.Generic;
using Test.Common.Entities;
using Test.Data.Rest;
using Test.DataContracts.Contracts;

namespace Test.Data.Rest.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private HttpService _httpService;
        public EmployeeRepository(HttpService httpService)
        {
            _httpService = httpService;
        }
        public List<Employee> Get()
        {
            try
            {
                var result = _httpService.getApiResult("Employees");
                var employeeList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Employee>>(result.Content);
                return employeeList;
            }
            catch (Exception e) {
                throw new Exception(e.Message);
            }
        }

        public Employee Get(int id)
        {
            return Get().Find(x => x.Id == id);
        }

        public Employee Insert(Employee entity)
        {
            throw new NotImplementedException();
        }

        public Employee Update(Employee entity)
        {
            throw new NotImplementedException();
        }
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
