﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Test.Data.Rest.Properties;
using System.Web.Mvc;
using RestSharp;

namespace Test.Data.Rest
{
   public class HttpService
    {
        public HttpService()
        {
         
        }
        private static string ConstructRequestUri(string action, ICollection<KeyValuePair<string, string>> parameters)
        {
            var requestUri = $"{Settings.Default.WebApiUrl}/{action}?";
            requestUri = parameters.Aggregate(requestUri, (current, parameter) => current + $"{parameter.Key}={System.Uri.EscapeDataString(parameter.Value)}&");
            return requestUri.TrimEnd('?').TrimEnd('&');
        }

        private static IRestResponse getResponse(Method method, string action, ICollection<KeyValuePair<string, string>> parameters)
        {
            if (parameters == null) parameters = new List<KeyValuePair<string, string>>();
            var requestUri = ConstructRequestUri(action, parameters);
            var client = new RestClient(requestUri);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            IRestResponse response = client.Execute(request);
            return response;
        }

        public IRestResponse getApiResult(string action, ICollection<KeyValuePair<string, string>> parameters = null)
        {
            return getResponse(Method.GET, action, parameters);            
        }

    }
}
